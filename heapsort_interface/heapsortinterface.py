import asyncio
import sys
import yaml
from argparse import ArgumentParser
from abc import ABC, abstractmethod

class HeapSortBase(ABC):
    """
    Define base requirments for a heap sort
    """
    @abstractmethod
    def _heapsort(self): pass

    @abstractmethod
    def _move_down_list(self): pass

    @abstractmethod
    def _swap(self): pass


class HeapSort(HeapSortBase):
    """
    Heap Sort interface
    :param a_list: list() of int() 
    """

    def __init__(self, a_list):
        self.a_list = a_list
        self.loop = asyncio.get_event_loop()
        self.a_future_sorted_list = self.loop.create_future()
    

    def _heapsort(self):
        # Converts a list to a Heap
        a_list = self.a_list
        length = len(a_list) - 1
        middle = length / 2
        for i in range(int(middle), -1, -1):
            # Verify list is sorted
            self._move_down_list(a_list, i, length)

        for i in range( length, 0, -1):
            if a_list[0] > a_list[i]:
                self._swap(a_list, 0, i)
                self._move_down_list(a_list, 0, i - 1)
        return a_list


    def _move_down_list(self, a_list, first, last):
        # Verify a list is a heap
        pos_left_child = 2 * first + 1
        while pos_left_child <= last:
            # is there a right_child? and is it > left_child.value
            if (pos_left_child < last) and (a_list[pos_left_child] < a_list[pos_left_child +1]):
                # go to next child
                pos_left_child += 1

            elif (a_list[pos_left_child] > a_list[first]):
                self._swap(a_list, pos_left_child, first)
                
                first = pos_left_child
                pos_left_child = 2 * first + 1

            else:
                return


    def _swap(self, a_list, pos_left_child, first):
        # Swap items in list
        temp = a_list[first]
        a_list[first] = a_list[pos_left_child]
        a_list[pos_left_child] = temp


    async def set_a_future_sorted_list(self):
        """
        Set the result for our future heap
        """
        return self.a_future_sorted_list.set_result(self._heapsort())

async def sort_list(a_list):
    heap_sort = HeapSort(a_list)	

    # Sort heap_sort.a_list
    heap_sort.loop.create_task(
        heap_sort.set_a_future_sorted_list()
    )

    # Set a_sorted_list when or future is complete
    a_sorted_list = await heap_sort.a_future_sorted_list

    print("Sorting complete")
    return a_sorted_list

def main(argv=None):
    """
    Manager for HeapSort Interface
    """
    parser = ArgumentParser()

    parser.add_argument("-c", "--config", dest="config_file", default=None, help="File containing a list()")

    args = parser.parse_args(argv or sys.argv[1:])

    if args.config_file is not None:
        with open(args.config_file) as handle:
            config = yaml.safe_load(handle)
        a_list = config.get('a_list')
    else:
        a_list = list([8,9,3,8,6,3,1,2,3,4,6,5,0,1,8,3,5])

    print("Loading")
    print(a_list)
    loop = asyncio.get_event_loop()
    a_sorted_list = loop.run_until_complete(sort_list(a_list))
    print(a_sorted_list)
    
    with open('/opt/heapsortinterface/result-file','w') as outfile:
        yaml.dump(a_sorted_list, outfile)
    outfile.close()

if __name__ == '__main__':
    main()
