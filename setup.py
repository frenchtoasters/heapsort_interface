from setuptools import setup, find_packages
import heapsort_interface

setup(
    name='heapsort_interface',
    version=heapsort_interface.__version__,
    author=heapsort_interface.__author__,
    description='Python interface for a heapsort of a list',
    long_description=open('README.md').read(),
    url='https://gitlab.com/frenchtoasters/heapsort_interface',
    keywords=['interface', 'sort', 'heapsort'],
    license=heapsort_interface.__license__,
    packages=find_packages(exclude=['*.test', '*.test.*']),
    include_package_data=True,
    install_requires=open('requirements.txt').readlines(),
    entry_points={
        'console_scripts': [
            'heapsortinterface=heapsort_interface.heapsortinterface:main'
        ]
    }
)
