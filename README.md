# Heap Sort Interface

This is a simple asynchronous interface which sorts a `list()` using the heap sort method.

***NOTE*** This project is built using Python 3.7.2

## Install Requirements

To install the requirements for this interace run the following:

```
(venv) toast@toast: ~/$ python -m pip install -r requirements.txt
```

## Configuration

This interface can run with or without a `config.yaml` file being passed to it. An example `config.yaml` looks like the following:

```
a_list: [3,2,1,4,5]
```

## Run

To run this interface you need to run:

```
(venv) toast@toast: ~/$ python heapsortinterface.py -c $CONFIG_FILE
``` 

## Binary

To install the package and run it as a python binary do the following:

```
(venv) toast@toast: ~/$ python setup.py install 
(venv) toast@toast: ~/$ heapsortinterface -c $CONFIG_FILE
```
